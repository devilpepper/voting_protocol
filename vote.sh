#!/bin/bash

n=$1

g++ server.cpp -o server
g++ client.cpp -o client
dirT=$(pwd)
read -s -p "Enter Lab Password(so insecure): " password
echo :p

tmux start-server
tmux new-session -d -s vote -n server
tmux select-window -t vote

#run server code
tmux send-keys "expect <<EOD" C-m
tmux send-keys "spawn ssh 134.74.160.101" C-m
tmux send-keys "expect password:" C-m
tmux send-keys "send $password\r" C-m
tmux send-keys "interact" C-m
tmux send-keys "EOD" C-m

#tmux select-window -t vote
tmux send-keys "cd $dirT" C-m
tmux send-keys "./server $n 1 2 1 0" C-m

ips=(102 103 104 105 106 107 108 109 110 112)

client=0
#run client code
for i in "${ips[@]}" 
do
	tmux new-session -d -s $i
	tmux select-window -t $i

	tmux send-keys "expect <<EOD" C-m
	tmux send-keys "spawn ssh 134.74.160.$i" C-m
	tmux send-keys "expect password:" C-m
	tmux send-keys "send $password\r" C-m
	tmux send-keys "interact" C-m
	tmux send-keys "EOD" C-m

	#tmux select-window -t $i
	tmux send-keys "cd $dirT" C-m
	tmux send-keys "./client $n $client 0" C-m

	((client++))
	if [[ client > n ]]; then
		break;
	fi
		
done
